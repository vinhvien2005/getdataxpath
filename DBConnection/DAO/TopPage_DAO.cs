﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBConnection.DTO;
using System.Data;
namespace DBConnection.DAO
{
    public class TopPage_DAO:MyConnection
    {
        const string TableName = "top_page";
       
        public int InsertTopPage(TopPage_DTO topPage)
        {
            return Insert(TableName, new string[] { "key_word", "status" }, new object[] { topPage.KeyWord, topPage.Status });    
        }
        public int UpdateTopPage(TopPage_DTO topPage,string []Fields,object[]Datas)
        {
            return Update(TableName, topPage.ID, Fields, Datas);    
        }
        public int DeleteTopPage(TopPage_DTO topPage)
        {
            return Delete(topPage.ID, TableName);
        }
        public List<TopPage_DTO> GetAllTopPageByStatus(int status)
        {
            string sql = string.Format("select * from top_page where status = {0}",status);
            DataSet result = GetData(sql);
            return GetByDataSet(result);
        }
        public List<TopPage_DTO> GetAllTopPage()
        {
            string sql = string.Format("select * from top_page");
            DataSet result = GetData(sql);
            return GetByDataSet(result);
        }
        public bool CheckExistTopPage(TopPage_DTO topPage)
        {
            string sql = string.Format("select * from top_page where key_word = '{0}'", topPage.KeyWord);
            DataSet result = GetData(sql);
            return (result.Tables.Count > 0 && result.Tables[0].Rows.Count > 0);
        }
        private List<TopPage_DTO> GetByDataSet(DataSet data)
        {
            List<TopPage_DTO> list = new List<TopPage_DTO>();
            if (data != null && data.Tables.Count > 0&& data.Tables[0].Rows.Count > 0 )
            {
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    list.Add(GetByRowData(data.Tables[0].Rows[i]));
                }
            }
            return list;
        }
        private TopPage_DTO GetByRowData(DataRow raw)
        {
            TopPage_DTO obj = new TopPage_DTO();
            if (raw != null)
            {
                obj.ID = Convert.ToInt32(raw["id"].ToString());
                obj.KeyWord = raw["key_word"].ToString();
                obj.Status = Convert.ToInt32(raw["status"].ToString());
                obj.InputDate = Convert.ToDateTime(raw["input_date"].ToString()).ToShortDateString();
                switch (obj.Status)
                {
                    case (int)TopPage_DTO.StatusTopPage.Todo: 
                        obj.StringStatus = "To do"; break;
                    case (int)TopPage_DTO.StatusTopPage.InProgressUrlListGenerate:
                        obj.StringStatus = "In Progress Url List generate"; break;
                    case (int)TopPage_DTO.StatusTopPage.InProgressAnchorGenerate:
                        obj.StringStatus = "In Progress Anchor generate"; break;
                    case (int)TopPage_DTO.StatusTopPage.Done:
                        obj.StringStatus = "Done"; break;
                }
            }
            return obj;
        }
    }
}
