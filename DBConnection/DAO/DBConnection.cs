﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.DAO
{
    public class MyConnection
    {

        private SqlConnection con = null;

        string strConnection = string.Empty;

        public MyConnection()
        {
            strConnection = System.Configuration.ConfigurationSettings.AppSettings["ConnectionString"].ToString();
        }
        private void OpenConnect()
        {
            con = new SqlConnection(strConnection);
            con.Open();
        }
        public int ExecuteNonQuery(SqlCommand cmd)
        {
            int number = -1;
            try
            {
                OpenConnect();
                cmd.Connection = con;
                number = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }
            return number;
        }
        public int ExecuteNonQuery(String strSQL)
        {

            int number = -1;
            SqlCommand cmd = null;
            try
            {
                OpenConnect();
                cmd = new SqlCommand(strSQL);
                cmd.Connection = con;
                number = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }
            return number;
        }
        public DataSet GetData(SqlCommand cmd)
        {

            DataSet ds = null;
            SqlDataAdapter adap = null;
            try
            {
                OpenConnect();
                cmd.Connection = con;
                ds = new DataSet();
                adap = new SqlDataAdapter(cmd);
                adap.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
                cmd.Dispose();
            }
            return ds;
        }
        public DataSet GetData(String strSQL)
        {

            DataSet ds = null;
            SqlDataAdapter adap = null;
            try
            {
                OpenConnect();
                ds = new DataSet();
                adap = new SqlDataAdapter(strSQL, con);
                adap.Fill(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
            return ds;
        }

        public int Insert(string TableName,string[] Fields, object[] Datas)
        {
            string StrInsert = "Insert into {0}({1}) values({2}) set @Identity=@@Identity";
            SqlCommand cmd = new SqlCommand();
            SqlParameter Identity = new SqlParameter("@Identity", 0);
            Identity.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(Identity);
            string StrField = "";
            string StrValue = "";
            for (int i = 0; i < Fields.Length; i++)
            {
                if (i > 0)
                {
                    StrField += ",";
                    StrValue += ",";
                }
                StrField += Fields[i];
                StrValue += "@" + Fields[i];
                cmd.Parameters.Add(new SqlParameter("@" + Fields[i], Datas[i]));
            }
            cmd.CommandText = string.Format(StrInsert, TableName, StrField, StrValue);

            int result = ExecuteNonQuery(cmd);
            if (result > 0 && Identity.Value != DBNull.Value)
                result = Convert.ToInt32(Identity.Value);
            return result;
        }

        public int Update(string TableName,int ValueID, string[] Fields, object[] Datas)
        {
            string StrUpdate = "Update " + TableName + " set {0} where id =" + ValueID;
            SqlCommand cmd = new SqlCommand();
            string strset = "";
            for (int i = 0; i < Fields.Length; i++)
            {
                if (i > 0)
                    strset += ",";
                strset += Fields[i] + "=@" + Fields[i];
                cmd.Parameters.Add(new SqlParameter("@" + Fields[i], Datas[i]));
            }
            cmd.CommandText = string.Format(StrUpdate, strset);
            return ExecuteNonQuery(cmd);
        }
        public int Delete(int ID,string TableName)
        {
            string strDelete = "Delete " + TableName + " where id=" + ID;
            return ExecuteNonQuery(strDelete);
        }

    }
}
