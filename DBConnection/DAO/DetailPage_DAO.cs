﻿using DBConnection.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DBConnection.DAO
{
    public class DetailPage_DAO:MyConnection
    {
        const string TableName = "detail_page";

        public int InsertDetailPage(DetailPage_DTO detailPage)
        {
            return Insert(TableName, new string[] { "url_id", "title", "meta_description" }, new object[] { detailPage.UrlId, detailPage.Title,detailPage.MetaDescription });
        }
        public int UpdateDetailPage(DetailPage_DTO detailPage, string[] Fields, object[] Datas)
        {
            return Update(TableName, detailPage.ID, Fields, Datas);
        }
        public int DeleteDetailPage(DetailPage_DTO detailPage)
        {
            return Delete(detailPage.ID, TableName);
        }
        public DetailPage_DTO GetDetailPageByUrlId(int UrlId)
        {
            string sql = string.Format("select * from detail_page where url_id = {0}", UrlId);
            DataSet result = GetData(sql);
            return GetByRowData(result.Tables[0].Rows[0]);
        }
        private List<DetailPage_DTO> GetByDataSet(DataSet data)
        {
            List<DetailPage_DTO> list = new List<DetailPage_DTO>();
            if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    list.Add(GetByRowData(data.Tables[0].Rows[i]));
                }
            }
            return list;
        }
        private DetailPage_DTO GetByRowData(DataRow raw)
        {
            DetailPage_DTO obj = new DetailPage_DTO();
            if (raw != null)
            {
                obj.ID = Convert.ToInt32(raw["id"].ToString());
                obj.UrlId = Convert.ToInt32(raw["url_id"].ToString());
                obj.Title = raw["title"].ToString();
                obj.MetaDescription = raw["meta_description"].ToString();
            }
            return obj;
        }
    }
}
