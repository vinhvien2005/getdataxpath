﻿using DBConnection.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.DAO
{
    public class UrlListPage_DAO:MyConnection
    {
        const string TableName = "url_list_page";

        public int InsertUrlListPage(UrlListPage_DTO urlListPage)
        {
            return Insert(TableName, new string[] { "top_page_id", "rank", "title", "url_value" }, new object[] { urlListPage.TopPageId, urlListPage.Rank, urlListPage.Title, urlListPage.UrlValue });
        }
        public int UpdateUrlListPage(UrlListPage_DTO urlListPage, string[] Fields, object[] Datas)
        {
            return Update(TableName, urlListPage.ID, Fields, Datas);
        }
        public int DeleteUrlListPage(TopPage_DTO urlListPage)
        {
            return Delete(urlListPage.ID, TableName);
        }
        public List<UrlListPage_DTO> GetAllUrlByTopPageId(int topPageId)
        {
            string sql = string.Format("select * from url_list_page where top_page_id = {0}", topPageId);
            DataSet result = GetData(sql);
            return GetByDataSet(result);
        }
        public UrlListPage_DTO GetUrlById(int id)
        {
            string sql = string.Format("select * from url_list_page where id = {0}", id);
            DataSet result = GetData(sql);
            return GetByRowData(result.Tables[0].Rows[0]);
        }
        private List<UrlListPage_DTO> GetByDataSet(DataSet data)
        {
            List<UrlListPage_DTO> list = new List<UrlListPage_DTO>();
            if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    list.Add(GetByRowData(data.Tables[0].Rows[i]));
                }
            }
            return list;
        }
        private UrlListPage_DTO GetByRowData(DataRow raw)
        {
            UrlListPage_DTO obj = new UrlListPage_DTO();
            if (raw != null)
            {
                obj.ID = Convert.ToInt32(raw["id"].ToString());
                obj.TopPageId = Convert.ToInt32(raw["top_page_id"].ToString());
                obj.Rank = Convert.ToInt32(raw["rank"].ToString());
                obj.Title = raw["title"].ToString();
                obj.UrlValue = raw["url_value"].ToString();
                obj.PageTitle = raw["page_title"].ToString();
                obj.MetaDescription = raw["meta_description"].ToString();
            }
            return obj;
        }
    }
}
