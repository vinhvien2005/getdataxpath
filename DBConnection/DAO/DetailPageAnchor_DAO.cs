﻿using DBConnection.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.DAO
{
    public class DetailPageAnchor_DAO:MyConnection
    {
        const string TableName = "detail_page_anchor";

        public int InsertDetailPageAnchor(DetailPageAnchor_DTO detailPageAnchor)
        {
            return Insert(TableName, new string[] { "url_id", "anchor_text", "anchor_type", "anchor_url" },
                new object[] { detailPageAnchor.UrlId, detailPageAnchor.AnchorText, detailPageAnchor.AnchorType, detailPageAnchor .AnchorUrl});
        }
        public int UpdateDetailPageAnchor(DetailPageAnchor_DTO detailPageAnchor, string[] Fields, object[] Datas)
        {
            return Update(TableName, detailPageAnchor.ID, Fields, Datas);
        }
        public int DeleteDetailPageAnchor(DetailPageAnchor_DTO detailPageAnchor)
        {
            return Delete(detailPageAnchor.ID, TableName);
        }

        public List<DetailPageAnchor_DTO> GetAllDetailPageAnchorByUrlId(int UrlId)
        {
            string sql = string.Format("select * from detail_page_anchor where url_id = {0}", UrlId);
            DataSet result = GetData(sql);
            return GetByDataSet(result);
        }
        private List<DetailPageAnchor_DTO> GetByDataSet(DataSet data)
        {
            List<DetailPageAnchor_DTO> list = new List<DetailPageAnchor_DTO>();
            if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < data.Tables[0].Rows.Count; i++)
                {
                    list.Add(GetByRowData(data.Tables[0].Rows[i]));
                }
            }
            return list;
        }
        private DetailPageAnchor_DTO GetByRowData(DataRow raw)
        {
            DetailPageAnchor_DTO obj = new DetailPageAnchor_DTO();
            if (raw != null)
            {
                obj.ID = Convert.ToInt32(raw["id"].ToString());
                obj.UrlId = Convert.ToInt32(raw["url_id"].ToString());
                obj.AnchorText = raw["anchor_text"].ToString();
                obj.AnchorType = raw["anchor_type"].ToString();
                obj.AnchorUrl = raw["anchor_url"].ToString();
            }
            return obj;
        }
    }
}
