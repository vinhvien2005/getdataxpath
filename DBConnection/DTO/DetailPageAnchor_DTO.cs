﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConnection.DTO
{
    public class DetailPageAnchor_DTO
    {
        public int ID { get;set; }
        public int UrlId { get; set; }
        public string AnchorText { get; set; }
        public string AnchorType { get; set; }
        public string AnchorUrl { get; set; }
    }
}