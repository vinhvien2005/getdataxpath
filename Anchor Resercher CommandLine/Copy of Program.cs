﻿using DBConnection.DAO;
using DBConnection.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Anchor_Resercher_CommandLine
{
    class Program
    {

        HtmlWeb myWeb = new HtmlWeb(); // get web result

        // link get data
        static string strGetDataLink = "https://www.google.co.jp/search?q={0}&start=0&gl=jp";

        // define regular
        static string getTitleUrlGoogle = "<h3 class='r'><a([^>]+)>(?<title>[^<]+)<";
        static string getUrlOfRankPage = "<h3 class='r'><a href='(?<link>[^']+)'";
        static string getTitleContent = "<title>(?<title>[^<]+)<";
        static string getMetaDescription = "<meta name='description' content='(?<description>[^']+)'";
        static string getATag = "<a\\s+(?:[^>]*?\\s+)?href='(?<href>[^']+)'([^>]+)>(?<content>[^<]+)<";
        static string getImgContent = "<img(?<image>[^>]+)>";
        static string getImgAlt = "alt='(?<alt>[^']+)'";
        static string getImgSrc = "src='(?<src>[^']+)'";

        // define DAO object
        static TopPage_DAO topPgeDAO = new TopPage_DAO();
        static UrlListPage_DAO urlListPageDAO = new UrlListPage_DAO();
        static DetailPage_DAO detailPageDAO = new DetailPage_DAO();
        static DetailPageAnchor_DAO detailPageAnchorDAO = new DetailPageAnchor_DAO();


        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "getUrl": GetUrlByKeyWord(); break;
                    case "getAnchor": GetAnchorDetail(); break;
                    default: Console.WriteLine(string.Format("the command : {0} doesn't supported!", args[0])); break;
                }
            }
            Console.WriteLine("Press any key to exist !");
            Console.ReadKey();
        }

        /// <summary>
        /// Get content website by input link.
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        public static string GetContentWebsite(string link)
        {
            string result = string.Empty;
                try
                {
                    System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(link);
                    request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36";
                    request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";

                    request.CookieContainer = new CookieContainer();
                    System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse();
                    System.IO.Stream ReceiveStream = response.GetResponseStream();
                    System.IO.StreamReader ReadStream = new System.IO.StreamReader(ReceiveStream, System.Text.Encoding.UTF8);
                    result = ReadStream.ReadToEnd();
                    result = result.Replace("\"", "\'");
                    response.Close();
                    ReceiveStream.Close();
                }
                catch (Exception ex) {

                    // log error when load content web
                    result = string.Empty;
                }
                return result;
        }

        /// <summary>
        /// load all top_page by status = 1
        /// for each top_page search keyword on google and get all link in firt page.
        /// change status top_page to 2.
        /// </summary>
        public static void GetUrlByKeyWord()
        {
            // get all top_page with status = 1
            List<TopPage_DTO> listTopPage = new List<TopPage_DTO>();
            try
            {
                listTopPage = topPgeDAO.GetAllTopPageByStatus((int)TopPage_DTO.StatusTopPage.Todo);
            }
            catch (Exception ex)
            {
                // log error when get top_page.
            }
            if (listTopPage.Count > 0)
            {
                for (int i = 0; i < listTopPage.Count; i++)
                {
                    // get all link and title of current top_page 
                    string googleUrl = string.Format(strGetDataLink, listTopPage[i].KeyWord);
                    string contentWeb = GetContentWebsite(googleUrl);
                    if (!string.IsNullOrEmpty(contentWeb))
                    {
                        // get title and url
                        MatchCollection matchTitle = Regex.Matches(contentWeb, getTitleUrlGoogle, RegexOptions.IgnoreCase);
                        MatchCollection matchUrl = Regex.Matches(contentWeb, getUrlOfRankPage, RegexOptions.IgnoreCase);
                        if (matchTitle.Count > 0)
                        {
                            List<UrlListPage_DTO> listUrlListPageDTO = new List<UrlListPage_DTO>();
                            for (int j = 0; j < matchUrl.Count; j++)
                            {
                                UrlListPage_DTO urlListPageDTO = new UrlListPage_DTO();
                                urlListPageDTO.TopPageId = listTopPage[i].ID;
                                urlListPageDTO.Title = matchTitle[j].Groups["title"].Value;
                                urlListPageDTO.UrlValue = matchUrl[j].Groups["link"].Value;
                                urlListPageDTO.Rank = j + 1;
                                listUrlListPageDTO.Add(urlListPageDTO);
                            }
                            // insert UrlListPage_DTO  into database
                            if (listUrlListPageDTO.Count > 0)
                            {
                                for (int k = 0; k < listUrlListPageDTO.Count; k++)
                                {
                                    try
                                    {
                                        urlListPageDAO.InsertUrlListPage(listUrlListPageDTO[k]);
                                    }
                                    catch (Exception ex)
                                    {
                                        // log error when insert UrlListPage.
                                    }

                                }
                            }
                        }
                        // can't get value of title and url 
                    }
                    // web content is null

                    // update status for current top_page to In Progress Url List generate
                    try
                    {
                        topPgeDAO.UpdateTopPage(listTopPage[i], new string[] { "status" }, new object[] { (int)TopPage_DTO.StatusTopPage.InProgressUrlListGenerate });
                    }
                    catch (Exception ex)
                    {
                        // log error when update status of TopPage.
                    }

                }
            }
            // don't have any top_page to get data
        }

        /// <summary>
        /// Get all list topPage by status = 2
        /// for each top_page 
        /// Update status top_page to 3
        /// for each url of Top_Page
        /// get necessary data.
        /// update status top_page to 4.
        /// </summary>
        public static void GetAnchorDetail()
        {
            try
            {
                // get all top_page with status is In Progress Url List generate - 2
                List<TopPage_DTO> listTopPage = topPgeDAO.GetAllTopPageByStatus((int)TopPage_DTO.StatusTopPage.InProgressUrlListGenerate);
                if (listTopPage.Count > 0)
                {
                    for (int i = 0; i < listTopPage.Count; i++)
                    {
                        // change status of top_page to In Progress Anchor generate - 3
                        int resultUpdate = 0;
                        try
                        {
                            resultUpdate = topPgeDAO.UpdateTopPage(listTopPage[i], new string[] { "status" }, new object[] { (int)TopPage_DTO.StatusTopPage.InProgressAnchorGenerate });
                        }
                        catch (Exception ex)
                        {
                            // log error when update top_page doesn't success !
                        }
                        if (resultUpdate > 0)
                        {
                            // get all url_list_page of top_page.
                            List<UrlListPage_DTO> listUrlListPage = urlListPageDAO.GetAllUrlByTopPageId(listTopPage[i].ID);
                            if (listUrlListPage.Count > 0)
                            {
                                for (int j = 0; j < listUrlListPage.Count; j++)
                                {
                                    // get data from url page list
                                    string contentWeb = GetContentWebsite(listUrlListPage[j].UrlValue);
                                    // get title and meta description 
                                    Match matchTitle = Regex.Match(contentWeb, getTitleContent, RegexOptions.IgnoreCase);
                                    Match matchMetaDescription = Regex.Match(contentWeb, getMetaDescription, RegexOptions.IgnoreCase);
                                    if (matchTitle.Success)
                                    {
                                        listUrlListPage[j].PageTitle = matchTitle.Groups["title"].Value;
                                    }
                                    if (matchMetaDescription.Success)
                                    {
                                        listUrlListPage[j].MetaDescription = matchMetaDescription.Groups["description"].Value;
                                    }
                                    // update page_title with meta_description of urlListPage.
                                    bool updateSuccess = false;
                                    try
                                    {
                                        updateSuccess = urlListPageDAO.UpdateUrlListPage(listUrlListPage[j], new string[] { "page_title", "meta_description" }, new object[] { listUrlListPage[j].PageTitle, listUrlListPage[j].MetaDescription }) > 0;
                                    }
                                    catch (Exception ex)
                                    {
                                        // log error when insert detail page.
                                    }
                                    if (updateSuccess)
                                    {
                                        // get all anchor of url
                                        List<DetailPageAnchor_DTO> listDetailPageAnchor = new List<DetailPageAnchor_DTO>();

                                        // get a tag
                                        MatchCollection matchATag = Regex.Matches(contentWeb, getATag, RegexOptions.IgnoreCase);
                                        if (matchATag.Count > 0)
                                        {
                                            for (int a = 0; a < matchATag.Count; a++)
                                            {
                                                DetailPageAnchor_DTO detailAnchorDTO = new DetailPageAnchor_DTO();
                                                detailAnchorDTO.UrlId = listUrlListPage[j].ID;
                                                detailAnchorDTO.AnchorType = "Text";
                                                detailAnchorDTO.AnchorUrl = matchATag[a].Groups["href"].Value;
                                                detailAnchorDTO.AnchorText = matchATag[a].Groups["content"].Value;
                                                listDetailPageAnchor.Add(detailAnchorDTO);
                                            }
                                        }// can't find any a tag

                                        // get img tag 
                                        MatchCollection matchImgContent = Regex.Matches(contentWeb, getImgContent, RegexOptions.IgnoreCase);
                                        if (matchImgContent.Count > 0)
                                        {
                                            for (int img = 0; img < matchImgContent.Count; img++)
                                            {
                                                // get src and alt of image tag
                                                string imageContent = matchImgContent[img].Groups["image"].Value;
                                                Match matchImgSrc = Regex.Match(imageContent, getImgSrc, RegexOptions.IgnoreCase);
                                                Match matchImgAlt = Regex.Match(imageContent, getImgAlt, RegexOptions.IgnoreCase);
                                                DetailPageAnchor_DTO detailAnchorDTO = new DetailPageAnchor_DTO();
                                                detailAnchorDTO.UrlId = listUrlListPage[j].ID;
                                                detailAnchorDTO.AnchorType = "Img";
                                                if (matchImgSrc.Success)
                                                {
                                                    detailAnchorDTO.AnchorUrl = matchImgSrc.Groups["src"].Value;
                                                }
                                                if (matchImgAlt.Success)
                                                {
                                                    detailAnchorDTO.AnchorText = matchImgAlt.Groups["alt"].Value;
                                                }
                                                listDetailPageAnchor.Add(detailAnchorDTO);
                                            }
                                        }// don't have any img tag in web content

                                        // insert Anchor into databasae
                                        if (listDetailPageAnchor.Count > 0)
                                        {
                                            for (int k = 0; k < listDetailPageAnchor.Count; k++)
                                            {
                                                try
                                                {
                                                    detailPageAnchorDAO.InsertDetailPageAnchor(listDetailPageAnchor[k]);
                                                }
                                                catch (Exception ex)
                                                {
                                                    // log error when insert anchor.
                                                }
                                            }
                                        }

                                    }// insert detail page doesn't success !
                                }
                            } // don't have any url to get detail

                            // update status to done of top_page
                            try
                            {
                                topPgeDAO.UpdateTopPage(listTopPage[i], new string[] { "status" }, new object[] { (int)TopPage_DTO.StatusTopPage.Done });
                            }
                            catch (Exception ex)
                            {
                                // log when update status top_page to done doesn't success !
                            }

                        }// update status doesn't success ! => stop with current top_page.

                    }
                } // don't have any top_page to get detail.
            }
            catch (Exception ex)
            { 
                // log error when exception occus.
            }

        }

        public void WriteLog()
        {
            //
        }
    }
}
