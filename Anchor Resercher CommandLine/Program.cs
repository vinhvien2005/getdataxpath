﻿using DBConnection.DAO;
using DBConnection.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Anchor_Resercher_CommandLine
{
    class Program
    {

       static HtmlWeb myWeb = new HtmlWeb(); // get web result

        // link get data
        static string strGetDataLink = "https://www.google.co.jp/search?q={0}&start=0&gl=jp";

        // define DAO object
        static TopPage_DAO topPgeDAO = new TopPage_DAO();
        static UrlListPage_DAO urlListPageDAO = new UrlListPage_DAO();
        static DetailPage_DAO detailPageDAO = new DetailPage_DAO();
        static DetailPageAnchor_DAO detailPageAnchorDAO = new DetailPageAnchor_DAO();


        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "getUrl": GetUrlByKeyWord(); break;
                    case "getAnchor": GetAnchorDetail(); break;
                    default: Console.WriteLine(string.Format("the command : {0} doesn't supported!", args[0])); break;
                }
            }
            Console.WriteLine("Press any key to exist !");
            Console.ReadKey();

            //HtmlDocument doc = myWeb.Load("https://www.youtube.com/watch?v=z4y-jRHdHqo");

        }

        /// <summary>
        /// load all top_page by status = 1
        /// for each top_page search keyword on google and get all link in firt page.
        /// change status top_page to 2.
        /// </summary>
        public static void GetUrlByKeyWord()
        {
            // get all top_page with status = 1
            List<TopPage_DTO> listTopPage = new List<TopPage_DTO>();
            try
            {
                listTopPage = topPgeDAO.GetAllTopPageByStatus((int)TopPage_DTO.StatusTopPage.Todo);
            }
            catch (Exception ex)
            {
                // log error when get top_page.
            }
            if (listTopPage.Count > 0)
            {
                for (int i = 0; i < listTopPage.Count; i++)
                {
                    // get all link and title of current top_page 
                    string googleUrl = string.Format(strGetDataLink, listTopPage[i].KeyWord);
                    HtmlDocument doc = new HtmlDocument();
                    try
                    {
                        doc = myWeb.Load(googleUrl);
                    }
                    catch (Exception ex)
                    {
                        // can't load this page.
                    }
                    // get all result match
                    HtmlNodeCollection listNodeDiv = doc.DocumentNode.SelectNodes("//div[@class='g']");
                    if (listNodeDiv.Count > 0)
                    {
                        List<UrlListPage_DTO> listUrlListPageDTO = new List<UrlListPage_DTO>();
                        for (int j = 0; j < listNodeDiv.Count; j++)
                        {
                            HtmlNode h3Node = listNodeDiv[j].SelectSingleNode("descendant::h3");
                            HtmlNode citeNode = listNodeDiv[j].SelectSingleNode("descendant::cite");
                            if (h3Node != null && citeNode != null)
                            {
                                // check cite is link 
                                if (Uri.IsWellFormedUriString(citeNode.InnerText, UriKind.RelativeOrAbsolute))
                                {
                                    UrlListPage_DTO urlListPageDTO = new UrlListPage_DTO();
                                    urlListPageDTO.TopPageId = listTopPage[i].ID;
                                    urlListPageDTO.Title = System.Web.HttpUtility.HtmlDecode(h3Node.InnerText);
                                    urlListPageDTO.UrlValue = citeNode.InnerText;
                                    urlListPageDTO.Rank = j + 1;
                                    listUrlListPageDTO.Add(urlListPageDTO);
                                }
                            }
                        }

                        if (listUrlListPageDTO.Count > 0)
                        {
                            for (int k = 0; k < listUrlListPageDTO.Count; k++)
                            {
                                try
                                {
                                    urlListPageDAO.InsertUrlListPage(listUrlListPageDTO[k]);
                                }
                                catch (Exception ex)
                                {
                                    // log error when insert UrlListPage.
                                }

                            }
                        }

                    } // don't have any result

                    // update status for current top_page to In Progress Url List generate
                    try
                    {
                        topPgeDAO.UpdateTopPage(listTopPage[i], new string[] { "status" }, new object[] { (int)TopPage_DTO.StatusTopPage.InProgressUrlListGenerate });
                    }
                    catch (Exception ex)
                    {
                        // log error when update status of TopPage.
                    }

                }
            }
            // don't have any top_page to get data
        }

        /// <summary>
        /// Get all list topPage by status = 2
        /// for each top_page 
        /// Update status top_page to 3
        /// for each url of Top_Page
        /// get necessary data.
        /// update status top_page to 4.
        /// </summary>
        public static void GetAnchorDetail()
        {
            try
            {
                // get all top_page with status is In Progress Url List generate - 2
                List<TopPage_DTO> listTopPage = topPgeDAO.GetAllTopPageByStatus((int)TopPage_DTO.StatusTopPage.InProgressUrlListGenerate);
                if (listTopPage.Count > 0)
                {
                    for (int i = 0; i < listTopPage.Count; i++)
                    {
                        // change status of top_page to In Progress Anchor generate - 3
                        int resultUpdate = 0;
                        try
                        {
                            resultUpdate = topPgeDAO.UpdateTopPage(listTopPage[i], new string[] { "status" }, new object[] { (int)TopPage_DTO.StatusTopPage.InProgressAnchorGenerate });
                        }
                        catch (Exception ex)
                        {
                            // log error when update top_page doesn't success !
                        }
                        if (resultUpdate > 0)
                        {
                            // get all url_list_page of top_page.
                            List<UrlListPage_DTO> listUrlListPage = urlListPageDAO.GetAllUrlByTopPageId(listTopPage[i].ID);
                            if (listUrlListPage.Count > 0)
                            {
                                for (int j = 0; j < listUrlListPage.Count; j++)
                                {
                                    // get data from url page list
                                   // string contentWeb = GetContentWebsite(listUrlListPage[j].UrlValue);
                                    try
                                    {
                                        HtmlDocument doc = new HtmlDocument();
                                        doc = myWeb.Load(listUrlListPage[j].UrlValue);

                                        // get title and meta description 
                                        HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                                        HtmlNode metaDescription = doc.DocumentNode.SelectSingleNode("//meta[@name='description']");

                                        listUrlListPage[j].PageTitle = title == null ? string.Empty : System.Web.HttpUtility.HtmlDecode(title.InnerText);
                                        listUrlListPage[j].MetaDescription = metaDescription == null ? string.Empty : System.Web.HttpUtility.HtmlDecode(metaDescription.Attributes["content"].Value);
                                        // update page_title with meta_description of urlListPage.
                                        bool updateSuccess = false;
                                        try
                                        {
                                            updateSuccess = urlListPageDAO.UpdateUrlListPage(listUrlListPage[j], new string[] { "page_title", "meta_description" }, new object[] { listUrlListPage[j].PageTitle, listUrlListPage[j].MetaDescription }) > 0;
                                        }
                                        catch (Exception ex)
                                        {
                                            // log error when insert detail page.
                                        }
                                        if (updateSuccess)
                                        {
                                            // get all anchor of url
                                            List<DetailPageAnchor_DTO> listDetailPageAnchor = new List<DetailPageAnchor_DTO>();

                                            // get a tag
                                            HtmlNodeCollection listTagA = doc.DocumentNode.SelectNodes("//a");
                                            if (listTagA.Count > 0)
                                            {
                                                for (int a = 0; a < listTagA.Count; a++)
                                                {
                                                    DetailPageAnchor_DTO detailAnchorDTO = new DetailPageAnchor_DTO();
                                                    detailAnchorDTO.UrlId = listUrlListPage[j].ID;
                                                    detailAnchorDTO.AnchorType = "Text";
                                                    detailAnchorDTO.AnchorUrl = listTagA[a].Attributes["href"] == null ?string.Empty: listTagA[a].Attributes["href"].Value.Trim();
                                                    detailAnchorDTO.AnchorText = System.Web.HttpUtility.HtmlDecode(listTagA[a].InnerText.Trim());
                                                    listDetailPageAnchor.Add(detailAnchorDTO);
                                                }
                                            }// can't find any a tag

                                            // get img tag 
                                            HtmlNodeCollection listTagImage = doc.DocumentNode.SelectNodes("//img");
                                            if (listTagImage.Count > 0)
                                            {
                                                for (int img = 0; img < listTagImage.Count; img++)
                                                {
                                                    // get src and alt of image tag
                                                    DetailPageAnchor_DTO detailAnchorDTO = new DetailPageAnchor_DTO();
                                                    detailAnchorDTO.UrlId = listUrlListPage[j].ID;
                                                    detailAnchorDTO.AnchorType = "Img";
                                                    detailAnchorDTO.AnchorUrl = listTagImage[img].Attributes["src"] == null ? string.Empty : listTagImage[img].Attributes["src"].Value;
                                                    detailAnchorDTO.AnchorText = listTagImage[img].Attributes["alt"] == null ? string.Empty : System.Web.HttpUtility.HtmlDecode(listTagImage[img].Attributes["alt"].Value);
                                                    listDetailPageAnchor.Add(detailAnchorDTO);
                                                }
                                            }// don't have any img tag in web content

                                            // insert Anchor into databasae
                                            if (listDetailPageAnchor.Count > 0)
                                            {
                                                for (int k = 0; k < listDetailPageAnchor.Count; k++)
                                                {
                                                    try
                                                    {
                                                        detailPageAnchorDAO.InsertDetailPageAnchor(listDetailPageAnchor[k]);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        // log error when insert anchor.
                                                    }
                                                }
                                            }

                                        }// insert detail page doesn't success !
                                    }
                                    catch (Exception ex)
                                    {
                                        //log error when can't get web page data.
                                    }
                                }
                            } // don't have any url to get detail

                            // update status to done of top_page
                            try
                            {
                                topPgeDAO.UpdateTopPage(listTopPage[i], new string[] { "status" }, new object[] { (int)TopPage_DTO.StatusTopPage.Done });
                            }
                            catch (Exception ex)
                            {
                                // log when update status top_page to done doesn't success !
                            }

                        }// update status doesn't success ! => stop with current top_page.

                    }
                } // don't have any top_page to get detail.
            }
            catch (Exception ex)
            { 
                // log error when exception occus.
            }

        }

        public void WriteLog()
        {
            //
        }
    }
}
